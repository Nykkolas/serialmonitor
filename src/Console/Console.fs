﻿module SerialMonitor.Console

open System

type ISerialCom =
    { EndOfChunk: string;
      ReadLine: unit -> string;
      GoStartPos: unit -> unit
    }

let sanitize str =
    str
    |> String.filter (fun c -> c <> '\r') 

let eraseAndDisplay (data:string) prev =
    let length = data.Length
    let blanks =
        match (prev, length) with
        | (p, l) when p <= l -> ""
        | (p, l) when p > l -> new string (' ', p - l )
        | _ -> ""
    sprintf "%s%s" data blanks |> Console.WriteLine
    length

let display serialCom prev =
    let data = serialCom.ReadLine () |> sanitize
    if data = serialCom.EndOfChunk then
        serialCom.GoStartPos ()
        0
    else
        eraseAndDisplay data prev
