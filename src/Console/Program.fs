﻿// Learn more about F# at http://fsharp.org

open System
open System.IO.Ports
open SerialMonitor.Console

let sp = new SerialPort ("/dev/tty.usbmodem641", 115200)
let startPos = Console.CursorTop        

let serialCom =
    { EndOfChunk = "<>";
      ReadLine = sp.ReadLine;
      GoStartPos = fun () -> Console.SetCursorPosition (0, startPos)
    }

try
    sp.Open ()
with
    | _ -> printfn "Erreur Open"

let stateDisplay acc e =
    display serialCom acc

sp.DataReceived
|> Event.scan stateDisplay 0
|> ignore

[<EntryPoint>]
let rec main argv =
    let c = System.Console.ReadKey()
    if c.Key = System.ConsoleKey.Q then
        sp.Close ()
        0
    else 
        Threading.Thread.Sleep (10)
        main argv
