﻿namespace Console.Tests

module Tests =
    open Expecto
    open Expecto.Flip
    open SerialMonitor.Console
    open TestHelpers
    open System.IO
    open System
    
    [<Tests>]
    let test =
        testSequenced <| testList "Display functions" [
           testCase "Empty input" (fun _ ->
                // Arrange
                let sw = setupConsoleOut ()
                let input = "\n"
                let serialCom = basicSerialCom input

                // Act
                display serialCom 0 |> ignore

                // Assert
                sw.ToString () |> Expect.equal "Empty string" input 
           );
           testCase "One line" (fun _ ->
                // Arrange
                let input = "Heading : 271\r\n"
                let expected = "Heading : 271\n"
                let sw = setupConsoleOut ()
                let serialCom = basicSerialCom input

                // Act
                display serialCom 0
                |> Expect.equal "Number of characters" (expected.Length - 1)

                // Assert           
                sw.ToString () |> Expect.equal "One line" expected 
           );
           testCase "Two lines" (fun _ ->
                // Arragne
                let prev = 13
                let input = "Heading : 1\r\n"
                let expected = "Heading : 1  \n"
                let sw = setupConsoleOut ()
                let serialCom = basicSerialCom input

                // Act
                display serialCom prev |> ignore

                // Assert
                sw.ToString () |> Expect.equal "Output only one line" expected 
            );
            testCase "Detect end of chunk" (fun _ ->
                //Arrange
                let input = "<>\r\n"
                let expected = ""
                let sw = setupConsoleOut ()
                let goStartAgent = GoStartAgent ()
                let serialCom = { basicSerialCom input with GoStartPos = goStartAgent.Call }

                // Act
                display serialCom 0 |> ignore

                // Assert
                goStartAgent.GetStaus |> Expect.isTrue "Go first line called" 
                sw.ToString () |> Expect.equal "End Of Chunk" expected
            );
        ]
