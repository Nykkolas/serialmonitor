﻿namespace Console.Tests

module TestHelpers =
    open System
    open System.IO
    open SerialMonitor.Console

    let setupConsoleOut () =
        let sw = new StringWriter ()
        Console.SetOut (TextWriter.Synchronized (sw))
        sw

    let basicSerialCom (str:string) =
        { EndOfChunk = "<>";
          ReadLine = fun () -> str.TrimEnd '\n';
          GoStartPos = fun () -> ()
        }

    type GoStartAgentMsg =
        | Call
        | GetStatus of AsyncReplyChannel<bool>

    type GoStartAgent () =
        let agent = MailboxProcessor.Start (fun inbox ->
            let rec messageLoop status = async {
                let! msg = inbox.Receive()
                match msg with
                | Call -> return! messageLoop true
                | GetStatus replyChannel ->
                    replyChannel.Reply status
                    return! messageLoop status
            }
            messageLoop false
        )

        member this.Call () = agent.Post Call

        member this.GetStaus = agent.PostAndReply (fun reply -> GetStatus reply)